package com.example.tony.partymaker.di.modules;


import com.example.tony.partymaker.network.PartyApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = {RetrofitModule.class})
public class ApiModule {
    @Provides
    @Singleton
    public PartyApi provideAuthApi(Retrofit retrofit) {
        return retrofit.create(PartyApi.class);
    }
}