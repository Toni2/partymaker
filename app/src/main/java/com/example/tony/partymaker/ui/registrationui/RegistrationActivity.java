package com.example.tony.partymaker.ui.registrationui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.tony.partymaker.R;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }
}
