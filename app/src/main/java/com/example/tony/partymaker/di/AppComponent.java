package com.example.tony.partymaker.di;


import android.content.Context;

import com.example.tony.partymaker.PartyService;
import com.example.tony.partymaker.di.modules.ContextModule;
import com.example.tony.partymaker.di.modules.PartyModule;
import com.example.tony.partymaker.mvp.presenter.DownloadDataPresenter;
import com.example.tony.partymaker.mvp.presenter.SignInPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ContextModule.class,PartyModule.class})
public interface AppComponent {
    Context getContext();
    PartyService getAuthService();


    void inject(SignInPresenter presenter);
    void injectDownloadData(DownloadDataPresenter downloadDataPresenter);



}