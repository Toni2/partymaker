package com.example.tony.partymaker;

import android.app.Application;
import android.support.annotation.VisibleForTesting;

import com.example.tony.partymaker.di.AppComponent;
import com.example.tony.partymaker.di.DaggerAppComponent;
import com.example.tony.partymaker.di.modules.ContextModule;

import io.reactivex.annotations.NonNull;

public class PartyApp extends Application {

    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        sAppComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .build();

    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    @VisibleForTesting
    public static void setAppComponent(@NonNull AppComponent appComponent) {
        sAppComponent = appComponent;
    }
}
