package com.example.tony.partymaker.mvp.presenter;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.tony.partymaker.PartyService;
import com.example.tony.partymaker.R;
import com.example.tony.partymaker.mvp.model.Login.Test;
import com.example.tony.partymaker.mvp.view.LoginView;
import com.example.tony.partymaker.PartyApp;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class SignInPresenter extends MvpPresenter<LoginView> {

    @Inject
    PartyService mPartyService;

    public SignInPresenter() {
        PartyApp.getAppComponent().inject(this);
    }

    public void signIn(String email, String password) {

        Integer emailError = null;
        Integer passwordError = null;

        getViewState().hideFormError();

        if (TextUtils.isEmpty(email)) {
            emailError = R.string.error_field_required;
        }

        if (TextUtils.isEmpty(password)) {
            passwordError = R.string.error_invalid_password;
        }

        if (emailError != null || passwordError != null) {
            getViewState().showFormError(emailError, passwordError);
            return;
        }

        getViewState().startSignIn();

        //   String credentials = String.format("%s:%s", email, password);

        //  final String token = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

        if (password.equals("123")) {
            password = "5fa285e1bebe0a6623e33afc04a1fbd5";
        }


        //  Observable<ApiFile> observable = retrofit.getApi("http://nbics.net/").uploadFileOnServerWithoutDraft(body, email, passwordHash);


        //   conversationActivity.visibleProgressBar();

        //  observable.subscribeOn(Schedulers.io())
        //          .observeOn(AndroidSchedulers.mainThread())
        //          .subscribe(new Observer<ApiFile>() {


        //  Observable observable = mPartyService.SignIn(email,password);


        Observable<Test> observable = mPartyService.SignIn(email, password);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Test>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Test test) {
                        getViewState().finishSignIn();
                        getViewState().successSignIn();
                    }


                    @Override
                    public void onError(Throwable e) {
                        getViewState().finishSignIn();
                        getViewState().failedSignIn(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    public void onErrorCancel() {
        getViewState().hideError();
    }
}
