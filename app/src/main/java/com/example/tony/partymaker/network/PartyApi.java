package com.example.tony.partymaker.network;

import com.example.tony.partymaker.mvp.model.Login.Test;
import com.example.tony.partymaker.mvp.model.Parties.Parties;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PartyApi {

    @GET("/bins/jjcth")
    Observable<Test> signIn(@Query("email") String login, @Query("PasswordHash") String passwordHash);

    @GET("/bins/17j29d")
    Observable <Parties> getParties();
}
