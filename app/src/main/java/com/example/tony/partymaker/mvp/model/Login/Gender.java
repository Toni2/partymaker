package com.example.tony.partymaker.mvp.model.Login;

import com.google.gson.annotations.SerializedName;

public class Gender {

    @SerializedName("Id")
    private Integer id;
    @SerializedName("Name")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
