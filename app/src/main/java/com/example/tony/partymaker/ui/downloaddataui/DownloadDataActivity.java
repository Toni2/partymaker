package com.example.tony.partymaker.ui.downloaddataui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.tony.partymaker.R;
import com.example.tony.partymaker.mvp.model.Parties.Party;
import com.example.tony.partymaker.mvp.presenter.DownloadDataPresenter;
import com.example.tony.partymaker.mvp.view.DownloadDataView;
import com.example.tony.partymaker.ui.partiesui.PartiesActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DownloadDataActivity extends MvpAppCompatActivity implements DownloadDataView {

    @InjectPresenter
    DownloadDataPresenter downloadDataPresenter;

    @BindView(R.id.progress_bar_download_data)
    ProgressBar mProgressDownloadDataView;

    private AlertDialog mErrorDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_data);
        ButterKnife.bind(this);

        attemptGetParties();


    }

    @Override
    protected void onDestroy() {
        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            mErrorDialog.setOnCancelListener(null);
            mErrorDialog.dismiss();
        }
        super.onDestroy();
    }

    @Override
    public void startDownloadData() {
        toggleProgressVisibility(true);
    }

    @Override
    public void finishDownloadData() {
        toggleProgressVisibility(false);
    }

    @Override
    public void hideErrorDownload() {
        if (mErrorDialog != null && mErrorDialog.isShowing()) {
            mErrorDialog.cancel();
        }
    }

    @Override
    public void successDownloadData(ArrayList<Party> party) {
        final Intent intent = new Intent(this, PartiesActivity.class);
        startActivity(intent);
    }

    private void toggleProgressVisibility(final boolean show) {
        mProgressDownloadDataView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void attemptGetParties() {
        downloadDataPresenter.getParties();

    }
}
