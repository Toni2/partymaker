package com.example.tony.partymaker.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.tony.partymaker.PartyService;
import com.example.tony.partymaker.mvp.model.Parties.Parties;
import com.example.tony.partymaker.mvp.model.Parties.Party;
import com.example.tony.partymaker.mvp.view.DownloadDataView;
import com.example.tony.partymaker.PartyApp;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class DownloadDataPresenter extends MvpPresenter<DownloadDataView> {

    @Inject
    PartyService mPartyService;

    public DownloadDataPresenter() {
        PartyApp.getAppComponent().injectDownloadData(this);
    }

    public void getParties() {
        Observable<Parties> observable = mPartyService.getParties();

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Parties>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Parties response) {
                        ArrayList<Party> parties = (ArrayList<Party>) response.getParty().clone();

                        getViewState().finishDownloadData();
                        getViewState().successDownloadData(parties);
                    }


                    @Override
                    public void onError(Throwable e) {
                        getViewState().finishDownloadData();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}