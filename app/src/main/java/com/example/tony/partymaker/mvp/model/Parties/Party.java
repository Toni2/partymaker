package com.example.tony.partymaker.mvp.model.Parties;

import com.google.gson.annotations.SerializedName;

public class Party {

    @SerializedName("nameParty")
    private String nameParty;
    @SerializedName("infoParty")
    private String infoParty;
    @SerializedName("maxPeoples")
    private String maxPeoples;

    public String getNameParty() {
        return nameParty;
    }

    public void setNameParty(String nameParty) {
        this.nameParty = nameParty;
    }

    public String getInfoParty() {
        return infoParty;
    }

    public void setInfoParty(String infoParty) {
        this.infoParty = infoParty;
    }

    public String getMaxPeoples() {
        return maxPeoples;
    }

    public void setMaxPeoples(String maxPeoples) {
        this.maxPeoples = maxPeoples;
    }

}

