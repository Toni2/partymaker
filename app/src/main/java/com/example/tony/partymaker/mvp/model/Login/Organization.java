package com.example.tony.partymaker.mvp.model.Login;

import com.google.gson.annotations.SerializedName;

public class Organization {

    @SerializedName("OrgType")
    private Integer orgType;
    @SerializedName("OrgId")
    private Integer orgId;
    @SerializedName("OrgIdOld")
    private Object orgIdOld;
    @SerializedName("OrgAlias")
    private String orgAlias;
    @SerializedName("OrgName")
    private String orgName;
    @SerializedName("OrgInn")
    private Object orgInn;
    @SerializedName("OrgKpp")
    private Object orgKpp;
    @SerializedName("IsDir")
    private Boolean isDir;
    @SerializedName("IsContactFace")
    private Boolean isContactFace;
    @SerializedName("Position")
    private String position;
    @SerializedName("Department")
    private String department;

    public Integer getOrgType() {
        return orgType;
    }

    public void setOrgType(Integer orgType) {
        this.orgType = orgType;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Object getOrgIdOld() {
        return orgIdOld;
    }

    public void setOrgIdOld(Object orgIdOld) {
        this.orgIdOld = orgIdOld;
    }

    public String getOrgAlias() {
        return orgAlias;
    }

    public void setOrgAlias(String orgAlias) {
        this.orgAlias = orgAlias;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Object getOrgInn() {
        return orgInn;
    }

    public void setOrgInn(Object orgInn) {
        this.orgInn = orgInn;
    }

    public Object getOrgKpp() {
        return orgKpp;
    }

    public void setOrgKpp(Object orgKpp) {
        this.orgKpp = orgKpp;
    }

    public Boolean getIsDir() {
        return isDir;
    }

    public void setIsDir(Boolean isDir) {
        this.isDir = isDir;
    }

    public Boolean getIsContactFace() {
        return isContactFace;
    }

    public void setIsContactFace(Boolean isContactFace) {
        this.isContactFace = isContactFace;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

}