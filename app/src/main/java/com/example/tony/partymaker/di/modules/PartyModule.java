package com.example.tony.partymaker.di.modules;

import com.example.tony.partymaker.PartyService;
import com.example.tony.partymaker.network.PartyApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApiModule.class})
public class PartyModule {

    @Provides
    @Singleton
    public PartyService providePartyService(PartyApi authApi) {
        return new PartyService(authApi);
    }
}
