package com.example.tony.partymaker.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.tony.partymaker.mvp.model.Parties.Party;

import java.util.ArrayList;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface DownloadDataView extends MvpView {

    void startDownloadData();

    void finishDownloadData();

    void hideErrorDownload();

    @StateStrategyType(SkipStrategy.class)
    void successDownloadData(ArrayList<Party> parties);

}
