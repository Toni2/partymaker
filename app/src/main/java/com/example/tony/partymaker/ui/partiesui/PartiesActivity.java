package com.example.tony.partymaker.ui.partiesui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.tony.partymaker.R;

public class PartiesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parties);
    }
}
