package com.example.tony.partymaker.mvp.model.Login;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Test {

    @SerializedName("Email")
    private String email;
    @SerializedName("UserStatus")
    private String userStatus;
    @SerializedName("FamilyName")
    private String familyName;
    @SerializedName("Name")
    private String name;
    @SerializedName("Patronymic")
    private String patronymic;
    @SerializedName("Phone")
    private String phone;
    @SerializedName("Skype")
    private String skype;
    @SerializedName("BirthDay")
    private String birthDay;
    @SerializedName("Icon")
    private Object icon;
    @SerializedName("IconUrl")
    private String iconUrl;
    @SerializedName("Gender")
    private Gender gender;
    @SerializedName("Entity")
    private Integer entity;
    @SerializedName("Organizations")
    private List<Organization> organizations = null;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public Object getIcon() {
        return icon;
    }

    public void setIcon(Object icon) {
        this.icon = icon;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Integer getEntity() {
        return entity;
    }

    public void setEntity(Integer entity) {
        this.entity = entity;
    }

    public List<Organization> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<Organization> organizations) {
        this.organizations = organizations;
    }

}
