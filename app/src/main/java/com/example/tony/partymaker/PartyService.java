package com.example.tony.partymaker;

import com.example.tony.partymaker.network.PartyApi;

import io.reactivex.Observable;

public class PartyService {

    private PartyApi partyApi;

    public PartyService(PartyApi partyApi) {
        this.partyApi = partyApi;
    }

    public Observable SignIn(String login, String passwordHash){
        return partyApi.signIn(login,passwordHash);
    }

    public Observable getParties(){
        return partyApi.getParties();
    }
}
