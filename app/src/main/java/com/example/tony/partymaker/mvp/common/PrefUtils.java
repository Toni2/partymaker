package com.example.tony.partymaker.mvp.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.tony.partymaker.PartyApp;

public class PrefUtils {

    private static final String PREF_NAME = "partymaker";

    public static SharedPreferences getPrefs() {
        return PartyApp.getAppComponent().getContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getEditor() {
        return getPrefs().edit();
    }
}
